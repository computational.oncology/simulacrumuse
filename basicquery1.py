#A basic Simulacrum script to demonstrate some usage
#Matt Williams 2018
#matthew.williams@imperial.ac.uk

import pandas as pd
from lifelines import KaplanMeierFitter
from lifelines.datasets import load_dd
data = load_dd()
#print(data.sample(6))

data_dir = "./simulacrum_release_v1.1.0/"
pat_file = data_dir + "sim_av_patient.csv"
tum_file = data_dir + "sim_av_tumour.csv"
regimen_file = data_dir + "sim_sact_regimen.csv"
patients = pd.read_csv(pat_file)
tumours = pd.read_csv(tum_file, low_memory=False)
regimen = pd.read_csv(regimen_file, low_memory=False, encoding = "ISO-8859-1")
print("*******************")

def isAliveasNum(InValue):
    print("Invalue: ", InValue, type(InValue))
    if str(InValue) == "A":
        return 1
    elif str(InValue) == "D":
        return 0

lung = tumours[tumours['SITE_ICD10_O2_3CHAR'] == 'C34']
#print(lung.count())
grp_morph = lung.groupby(['MORPH_ICD10_O2','STAGE_BEST'])
grp_behaviour = lung.groupby(['MORPH_ICD10_O2','BEHAVIOUR_ICD10_O2'])
#print(grp_morph['TUMOURID'].count())
#print(grp_behaviour['TUMOURID'].count())

lung_malignant = lung[lung['BEHAVIOUR_ICD10_O2'] == '3']
malig_grp = lung_malignant.groupby(['MORPH_ICD10_O2'])
#print(malig_grp['TUMOURID'].count())

#print(patients.head(3))
#print(lung_malignant.head(3))
#Now we merge data on lung cancers and patients
#And then drop breast and prostate cancer scores
lung_cancer_patients = pd.merge(patients, lung_malignant, on='PATIENTID')
lung_CP_reduced = lung_cancer_patients.drop(["GLEASON_TERTIARY", "GLEASON_COMBINED", "ER_SCORE",   "PR_SCORE", "ER_STATUS", "PR_STATUS", "GLEASON_PRIMARY",  "GLEASON_SECONDARY", "HER2_STATUS"], axis = 1)
print(lung_CP_reduced.count())
print(lung_CP_reduced.VITALSTATUSDATE.unique())
tmp = lung_cancer_patients[["VITALSTATUSDATE", "DIAGNOSISDATEBEST", "NEWVITALSTATUS"]]
print("\nTmp:\n" +str(tmp.head(3)))
tmp2 = pd.DataFrame({"STDate": pd.to_datetime(tmp["VITALSTATUSDATE"]), "DiagDate": pd.to_datetime(tmp["DIAGNOSISDATEBEST"])})
tmp2["SurvInt"] = tmp2["STDate"] - tmp2["DiagDate"]
tmp2["NEWVITALSTATUS"] = tmp["NEWVITALSTATUS"]
tmp2["NumVital"] = (tmp2["NEWVITALSTATUS"] == "D")
#print("\ntmp2:\n" + str(tmp2.head(3)))
tmp3 = tmp2.head(5)
tmp3["NumSurvInt"] = (tmp3["SurvInt"].dt.days)
print(tmp3)
# print(tmp3.dtypes)
# print(tmp3.describe())
# print(tmp3.notna().count())
#This line doesn't work as not applied across column
#tmp2["Num"] = isAliveasNum(tmp2["NEWVITALSTATUS"])


# lung_patients["StatusDate"] = pd.to_datetime(lung_patients["VITALSTATUSDATE"])
# lung_patients["DiagDate"] = pd.to_datetime(lung_patients["DIAGNOSISDATEBEST"])
# lung_patients["SurvInt"] = lung_patients["StatusDate"] - lung_patients["DiagDate"]

#print(lung_patients.dtypes)
#print(lung_patients.head(3))

#alive_dead = lung_patients.groupby('NEWVITALSTATUS').count()
#print("\nAD:\n" + str(alive_dead['PATIENTID']))

#print(lung_patients.groupby(["SurvInt"]).count())
#print(lung_patients["SurvInt"].describe())
#print(lung_patients.groupby(["NEWVITALSTATUS"]).count())
kmf = KaplanMeierFitter()
kmf.fit(tmp3["NumSurvInt"], (tmp3["NumVital"]))
kmf.plot()
#kmf.fit(lung_patients["SurvInt"], (lung_patients["NEWVITALSTATUS"] == "D"))
#print(kmf)
#tips[(tips['size'] >= 5) | (tips['total_bill'] > 45)]
#grp = tumours.groupby(['SITE_ICD10_O2_3CHAR'])
#grp2 = grp['SITE_ICD10_O2_3CHAR']


#ased on the “SIM_AV_TUMOUR” table, we have 102,350 rows, for 101,562 patient ids. This is my query:
#SELECT COUNT(DISTINCT(PATIENTID)) FROM SIM_AV_TUMOUR WHERE SITE_ICD10_O2_3CHAR = 'C34'

#This line needs fixing - the 'in' doesn't work
#sclc_lung = lung[lung['MORPH_ICD10_O2'] in ('8041', '8042','8043','8044','8045')]
#nsclc = lung[lung['MORPH_ICD10_O2'] == '8046']
#non_nsclc = lung[lung['MORPH_ICD10_O2'] != '8046']

#nsclc_malig = nsclc[nsclc['BEHAVIOUR_ICD10_O2'] == '3']
#print(nsclc_malig.head(3))
#print(nsclc_malig.describe())
#print(len(nsclc_malig))
#print(len(tumours))
#sim_av_tumour.csv
#sim_av_patient.csv
#sim_sact_cycle.csv
#sim_sact_drug_detail.csv
#sim_sact_outcome.csv
#sim_sact_patient.csv
#sim_sact_regimen.csv
#sim_sact_tumour.csv