This repository contains some simple code to demonstrate the use of the Simulacrum data

For more data on the Simulacrum, and for access to the data, please see below.

All code is provided "as is" and is licensed under GPLv3

Blog: https://computational-medicine.com/blog/ 

Website: https://simulacrum.healthdatainsight.org.uk/ (incl data download)

 

Poster: http://abstracts.ncri.org.uk/abstract/faking-it-building-a-simulacrum-of-non-identifiable-modelled-cancer-data-to-support-research/


 

Slides: https://20tqtx36s1la18rvn82wcmpn-wpengine.netdna-ssl.com/wp-content/uploads/2017/07/Building-a-Simulacrum-of-Non-Identifiable-Modelled-Cancer-Data-to-Support-Research-Eden.pdf